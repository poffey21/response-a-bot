from envparse import Env
from gitlab import Gitlab as GitLab
import os

env = Env()
if os.path.isfile('.env'):
    env.read_envfile('.env')


def main():
    private_token = env('PRIVATE_TOKEN')
    origin = env('GITLAB_HOST')
    project_ids = env('PROJECT_IDS', cast=list, subcast=int, default=[])
    project_ids_ignored = env('PROJECT_IDS_IGNORED', cast=list, subcast=int, default=[])
    group_ids = env('GROUP_IDS', cast=list, subcast=int, default=[])
    label_to_apply = env('LABEL_TO_APPLY', cast=str, default=False)
    gl = GitLab(url=origin, private_token=private_token)
    gl.auth()

    if group_ids:
        projects = set()
        for group_id in group_ids:
            group_projects = gl.groups.get(group_id).projects.list(all=True)
            for project in group_projects:
                projects.add(gl.projects.get(project.id))
        projects = list(projects)
    elif project_ids:
        projects = [gl.projects.get(x) for x in project_ids]
    else:
        projects = gl.projects.list(all=True)

    print(f"Looking for unresolved issues across {len(projects)} projects")
    for project in projects:
        if project.id in project_ids_ignored:
            continue
        issues = project.issues.list(all=True, status='opened')

        for issue in issues:

            newest_note = None
            assignee_ids = [x['id'] for x in issue.assignees]

            notes = issue.notes.list(all=True)
            for note in notes:
                if not hasattr(newest_note, 'created_at') or note.created_at > newest_note.created_at:
                    newest_note = note
            if not hasattr(newest_note, 'author') or newest_note.author['id'] in assignee_ids or not assignee_ids:
                if label_to_apply in issue.labels:
                    issue.labels = [x for x in issue.labels if x != label_to_apply]
                    issue.save()
                    print('removing label for', issue.title)
                continue
            if label_to_apply not in issue.labels:
                issue.labels.append(label_to_apply)
                issue.save()
                print('adding label for', issue.title)




if __name__ == "__main__":
    main()
