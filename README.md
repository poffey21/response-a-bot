# Response a Bot

Add a label to issues when the assignee has not left the most recent comment.

#### How To Use

1. Clone or fork repository into your own namespace
1. Go to `CI/CD` > `Pipelines` > `Run Pipeline`
1. Provide a GitLab Instance via the `GITLAB_HOST` and a token via `PRIVATE_TOKEN` (and other default variables)
1. Voila! The resulting pipeline will add a label to all issues awaiting a response.